import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    //for landing or home page 
    path : '',      
    loadChildren:() => import('./modules/landing-page/landing-page.module').then(m => m.LandingPageModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
