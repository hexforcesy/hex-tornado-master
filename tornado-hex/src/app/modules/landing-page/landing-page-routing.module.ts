import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ComplianceComponent } from './compliance/compliance.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { StatsComponent } from '../landing-page/stats/stats.component';

const routes: Routes = [
  {
    path: '',
    component : LandingPageComponent
  },
  {
    path: 'compliance',
    component : ComplianceComponent
  },
  {
    path: 'tutorial',
    component : TutorialComponent
  },
  {
    path: 'stats',
    component : StatsComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }
