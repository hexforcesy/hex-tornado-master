import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LandingPageRoutingModule } from './landing-page-routing.module';
import { ComplianceComponent } from './compliance/compliance.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { StatsComponent } from '../landing-page/stats/stats.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [LandingPageComponent, ComplianceComponent, TutorialComponent, StatsComponent],
  imports: [
    CommonModule,
    SharedModule,
    LandingPageRoutingModule,
    ChartsModule,
    FormsModule
  ]
})
export class LandingPageModule { }
