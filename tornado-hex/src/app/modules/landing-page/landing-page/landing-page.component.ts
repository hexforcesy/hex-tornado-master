import { Component, OnInit } from '@angular/core';
import { Web3Service} from 'src/app/shared/services /web3.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  providers: [Web3Service]
})
export class LandingPageComponent implements OnInit {
  tabStatus: boolean;
  address: any;
  balance: any;
  connectMetaMask: boolean;
  data: any;
  deposits = [];
  firstHalf: any[];
  secondHalf: any[];
  count: number;
  ip: any;
  radioStatus: any;
  noteString: string;
  noteBackup: boolean;
  noteArray: { noteString: string; commitment: string; }[];
  commitment: string;
  donationAddress: any;
  tokenBalance: any;
  account: boolean;
  recipient = '';
  note: any;
  showArticle: boolean;
  lowBalance: boolean;

  constructor(private web3Service: Web3Service) {}

  async ngOnInit(): Promise<void> {
    let that = this
    this.tabStatus = false;
    this.showArticle = true
    this.changeStatus('10000','0x7f6892edf9a54fa43db57ca52ee376ba2cdc1c1e')
    if(JSON.parse(localStorage.getItem('connected'))){
      this.connectMetaMask = true
    } else {
      this.connectMetaMask = false
    }

    //fetching IP address
    this.web3Service.getIPAddress().subscribe(res => {
      if(res){
        this.ip = res['ip']
      }
    },err => {
      return err
    })

    await that.web3Service.init()

    const address = await that.web3Service.getUserBalance().
    then(function(retAccount: any) {
      that.address = retAccount.account;
      return that.address
    }).catch(function(error) {
      console.log(error);
    });
    console.log(address);

    this.tokenBalance = await this.web3Service.getTokenBalance(address) / 100000000
   
    console.log(this.tokenBalance)
    
    // this.commonService.clearLocalStorage()
  }
  close(){
    this.showArticle = false
  }
  changeStatus(value, address){
    console.log(value)
    this.radioStatus = value

    //etherscan data api call
    this.web3Service.etherscanData(address).subscribe(res => {
      if(res){
        this.data = res['result']
        this.deposits = []
        for(let i=0; i < this.data.length; i++){
          if(this.data[i]['to'] === address){
            this.deposits.push(this.data[i])
          }
        }
        console.log(this.deposits)
        this.count = this.deposits.length
        this.firstHalf = this.deposits.slice(0 , 5)
        this.secondHalf = this.deposits.slice(5, 10)
      }
    }, err => {
      console.log(err)
    })
  }
  donate(val){
    this.donationAddress = val
  }
  async connect() {
    const connection = await this.web3Service.connect()  
    localStorage.setItem('connected', connection)
    if(connection){
       this.connectMetaMask = true
       location.reload()
    } else {
      this.connectMetaMask = false
    }
  }
  changeTab(tab) {
    if (tab === 'deposit') {
      this.tabStatus = false;


    } else if (tab === 'withdraw') {
      this.tabStatus = true;

    }
  }
 
  noteBackedup(){
    this.noteBackup = true
  }
  createDeposit() {
    this.web3Service.tornados(this.radioStatus)   
    if(this.tokenBalance < this.radioStatus){
      this.lowBalance = true
    } else {
      this.lowBalance = false
    }     
    this.noteArray =  this.web3Service.noteString()
    this.noteString = this.noteArray[0]['noteString']
    this.commitment = this.noteArray[0]['commitment']
    console.log(this.noteString)
  }
  async sendDeposit(){
    if(this.noteBackup){
      await this.web3Service.depositErc20(this.commitment)
    } else {

    }

  }
  async withdraw() {
    console.log(this.recipient)
    if(this.recipient && this.note){
      await this.web3Service.withdrawErc20(this.note, this.recipient)
    }
  }

}
