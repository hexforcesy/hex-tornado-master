import { Component, OnInit, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, BaseChartDirective } from 'ng2-charts';
import { Web3Service } from 'src/app/shared/services /web3.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
  providers: [Web3Service]
})
export class StatsComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['10k HEX', '100k HEX', '1000k HEX'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'desposits' },
    { data: [], label: 'withdrawals' }
  ];
  
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    annotation: {
      annotations: [],
    },
  };
  public lineChartLabels: Label[] = ['10k HEX', '100k HEX', '1000k HEX'];
  public lineChartType: ChartType = 'line';
  public lineChartLegend = true;
  public lineChartPlugins = [];

  public lineChartColors = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
 
  public lineChartData: ChartDataSets[] = [
    { data: [] , label: 'desposits' },
    { data: [], label: 'withdrawals' }
  ];


  tenKDeposits: any[];
  data: any;
  tenKAddress = '0x7f6892edf9a54fa43db57ca52ee376ba2cdc1c1e'
  hundredKAddress = '0x8cb144e93b78baad45882f30dcd0a21f8de82ec5'
  millionKAddress = '0x884ba9cdccc0dbd4d5283fd02181613a54bdbb24'
  tenKDCount: number;
  tenKWithdraw: any;
  tenKWCount: number;
  hundredKDeposits: any[];
  hundredKWithdraw: any[];
  hundredKDCount: number;
  hundredKWCount: number;
  millionKDeposits: any[];
  millionKWithdraw: any[];
  millionKDCount: number;
  millionKWCount: number;
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>

  constructor(private web3Service: Web3Service) { }
  ngOnInit(){
    //etherscan data api call
    
    this.web3Service.etherscanData(this.tenKAddress).subscribe(res => {
      if(res){
        this.data = res['result']
        this.tenKDeposits = []
        this.tenKWithdraw = []
        for(let i=0; i < this.data.length; i++){
          if(this.data[i]['to'] === this.tenKAddress){
            this.tenKDeposits.push(this.data[i])
          } else if(this.data[i]['from'] === this.tenKAddress){
            this.tenKWithdraw.push(this.data[i])
          }
        }
        this.tenKDCount = this.tenKDeposits.length
        this.lineChartData[0].data[0] = this.tenKDCount
        this.barChartData[0].data[0] = this.tenKDCount
        this.tenKWCount = this.tenKWithdraw.length
        this.lineChartData[1].data[0] = this.tenKWCount
        this.barChartData[1].data[0] = this.tenKWCount
        this.charts.forEach((c) => {
          c.update()
      });
      }
    }, err => {
      console.log(err)
    })

    this.web3Service.etherscanData(this.hundredKAddress).subscribe(res => {
      if(res){
        this.data = res['result']
        this.hundredKDeposits = []
        this.hundredKWithdraw = []
        for(let i=0; i < this.data.length; i++){
          if(this.data[i]['to'] === this.hundredKAddress){
            this.hundredKDeposits.push(this.data[i])
          } else if(this.data[i]['from'] === this.hundredKAddress){
            this.hundredKWithdraw.push(this.data[i])
          }
        }
        this.hundredKDCount = this.hundredKDeposits.length
        this.lineChartData[0].data[1] = this.hundredKDCount
        this.barChartData[0].data[1] = this.hundredKDCount
        this.hundredKWCount = this.hundredKWithdraw.length
        this.lineChartData[1].data[1] = this.hundredKWCount
        this.barChartData[1].data[1] = this.hundredKWCount
        this.charts.forEach((c) => {
          c.update()
      });
      }
    }, err => {
      console.log(err)
    })

    this.web3Service.etherscanData(this.millionKAddress).subscribe(res => {
      if(res){
        this.data = res['result']
        this.millionKDeposits = []
        this.millionKWithdraw = []
        for(let i=0; i < this.data.length; i++){
          if(this.data[i]['to'] === this.millionKAddress){
            this.millionKDeposits.push(this.data[i])
          } else if(this.data[i]['from'] === this.millionKAddress){
            this.millionKWithdraw.push(this.data[i])
          }
        }
        this.millionKDCount = this.millionKDeposits.length
        this.lineChartData[0].data[2] = this.millionKDCount
        this.barChartData[0].data[2] = this.millionKDCount
        this.millionKWCount = this.millionKWithdraw.length
        this.lineChartData[1].data[2] = this.millionKWCount
        this.barChartData[1].data[2] = this.millionKWCount
        this.charts.forEach((c) => {
          c.update()
      });
      }
    }, err => {
      console.log(err)
    })
    
  }
  

}
