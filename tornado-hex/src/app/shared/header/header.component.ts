import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Web3Service } from '../services /web3.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [Web3Service]
})
export class HeaderComponent implements OnInit {
  connectMetaMask: boolean;
  navItem: any;
  tokenBalance: any;
  address: any;

  constructor(private router: Router, private web3Service: Web3Service) { }

  ngOnInit() {
    if(JSON.parse(localStorage.getItem('connected'))){
      this.connectMetaMask = true
    } else {
      this.connectMetaMask = false
    }
  }

  changeNavItem(value) {
    this.navItem = value
  }

  async connect() {
    const connection = await this.web3Service.connect()  
    localStorage.setItem('connected', connection)
    if(connection){
       this.connectMetaMask = true
       location.reload()
    } else {
      this.connectMetaMask = false
    }
  }

  logout() {
    this.connectMetaMask = false 
    localStorage.clear();
    location.reload()
  }
}
