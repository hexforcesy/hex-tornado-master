import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import Web3 from 'web3';
import * as assert from 'assert';

// const fs = require('fs')
const snarkjs = require('snarkjs')
const crypto = require('crypto')
const circomlib = require('circomlib')
const bigInt = snarkjs.bigInt
const websnarkUtils = require('websnark/src/utils')
const merkleTree = require('../../../../../lib/MerkleTree')
const buildGroth16 = require('websnark/src/groth16')

const circuit = require('../../../../../build/circuits/withdraw.json')
const erc20ContractJson = require('../../../../../build/contracts/ERC20Mock.json');
const erc20tornadoJson = require('../../../../../build/contracts/ERC20Tornado.json')
const erc20DenominationsJson = require('../../../../../scripts/tornado-deployments.json');

declare let window: any;

@Injectable()
export class Web3Service {

  public web3: any;
  private enable: any;
  private account: any;
  public connectMetaMask: any;
  public erc20
  public erc20tornado
  public groth16
  /** Generate random number of specified byte length */
  rbigint = nbytes => snarkjs.bigInt.leBuff2int(crypto.randomBytes(nbytes))

  /** Compute pedersen hash */
  pedersenHash = data => circomlib.babyJub.unpackPoint(circomlib.pedersenHash.hash(data))[0]
  millionHex: any;
  hundredKHex: any;
  tenKHex: any;

  constructor(private readonly httpClient: HttpClient) { }

  public getIPAddress() {
    return this.httpClient.get("http://api.ipify.org/?format=json");
  }

  etherscanData(address) {
    return this.httpClient.get(`http://api.etherscan.io/api?module=account&action=tokentx&address=${address}&sort=desc&apikey=ID92D2RDWTU63M4HAB1UPT6J7XBDEG8MX6`);
  }

  trxCount(startDate, endDate) {
    return this.httpClient.get(`http://api.etherscan.io/api?module=stats&action=dailytx&startdate=${startDate}&enddate=${endDate}&sort=desc&apikey=ID92D2RDWTU63M4HAB1UPT6J7XBDEG8MX6`);
  }

  async connect() {
    if (window.ethereum === undefined) {
      alert('Non-Ethereum browser detected. Install MetaMask');
    } else {
      if (typeof window.web3 !== 'undefined') {
        this.web3 = window.web3.currentProvider;
      } else {
        this.web3 = new Web3.providers.HttpProvider('http://localhost:8545');
      }
      window.web3 = new Web3(window.ethereum);
      try {
        await window.ethereum.enable();
        return true
      } catch (error) {
        return error
      }
    }
  }

  public async getAccount(): Promise<any> {
    let that = this
    if (that.account == null) {
      that.account = await new Promise((resolve, reject) => {
        window.web3.eth.getAccounts((err, retAccount) => {
          console.log(retAccount);
          if (retAccount.length > 0) {
            that.account = retAccount[0];
            console.log(that.account)
            resolve(that.account);
          } else {
            // alert('transfer.service :: getAccount :: no accounts found.');
            reject('No accounts found.');
          }
          if (err != null) {
            alert('transfer.service :: getAccount :: error retrieving account');
            reject('Error retrieving account');
          }
        });
      }) as Promise<any>;
    }
    return Promise.resolve(that.account);
  }

  public async getUserBalance(): Promise<any> {
    const account = await this.getAccount();
    console.log('transfer.service :: getUserBalance :: account');
    console.log(account);
    return new Promise((resolve, reject) => {
      window.web3.eth.getBalance(account, function (err, balance) {
        console.log('transfer.service :: getUserBalance :: getBalance');
        console.log(balance);
        if (!err) {
          const retVal = {
            account: account,
            balance: balance
          };
          console.log('transfer.service :: getUserBalance :: getBalance :: retVal');
          console.log(retVal);
          resolve(retVal);
        } else {
          reject({ account: 'error', balance: 0 });
        }
      });
    }) as Promise<any>;
  }

  async getTokenBalance(account) {
    const that = this;
    console.log('account: ', account);

    console.log(that.erc20)
    let balance = await that.erc20.methods.balanceOf(account).call()
    return balance
  }

  toHex(number, length = 32) {
    let str = number instanceof Buffer ? number.toString('hex') : bigInt(number).toString(16)
    return '0x' + str.padStart(length * 2, '0')
  }

  createDeposit(nullifier, secret) {
    let deposit = { nullifier, secret }
    deposit['preimage'] = Buffer.concat([deposit.nullifier.leInt2Buff(31), deposit.secret.leInt2Buff(31)])
    deposit['commitment'] = this.pedersenHash(deposit['preimage'])
    deposit['nullifierHash'] = this.pedersenHash(deposit.nullifier.leInt2Buff(31))
    return deposit
  }
  async init() {
    const that = this;
    const web3 = new Web3(window.web3.currentProvider)
    
    that.erc20 = new web3.eth.Contract(erc20ContractJson.abi, erc20ContractJson.networks[1].address)
    const tx2 = await web3.eth.getTransaction(erc20ContractJson.networks[1].transactionHash)
    that.erc20['deployedBlock'] = tx2.blockNumber
    const tx3 = await web3.eth.getTransaction(erc20DenominationsJson["10kHex"].transactionHash);
    that.tenKHex = new web3.eth.Contract(erc20tornadoJson.abi, erc20DenominationsJson["10kHex"].address);
    that.tenKHex['deployedBlock'] = tx3.blockNumber;
    that.tenKHex['tokenAmount'] = erc20DenominationsJson["10kHex"].tokenAmount;
    
    const tx4 = await web3.eth.getTransaction(erc20DenominationsJson["100kHex"].transactionHash);
    that.hundredKHex = new web3.eth.Contract(erc20tornadoJson.abi, erc20DenominationsJson["100kHex"].address);
    that.hundredKHex['deployedBlock'] = tx4.blockNumber;
    that.hundredKHex['tokenAmount'] = erc20DenominationsJson["100kHex"].tokenAmount;
   
    const tx5 = await web3.eth.getTransaction(erc20DenominationsJson["1MHex"].transactionHash);
    that.millionHex = new web3.eth.Contract(erc20tornadoJson.abi, erc20DenominationsJson["1MHex"].address);
    that.millionHex['deployedBlock'] = tx5.blockNumber;
    that.millionHex['tokenAmount'] = erc20DenominationsJson["1MHex"].tokenAmount;
    
    console.log("coins tornado", that.erc20)
  }
  tornados(erc20){
     if (erc20 === '10000') {
       this.erc20tornado = this.tenKHex
     } else if (erc20 === '100000') {
      this.erc20tornado = this.hundredKHex
     } else {
       this.erc20tornado = this.millionHex
     }
    console.log("coins tornado", this.erc20tornado)

  }

  noteString() {
    const deposit = this.createDeposit(this.rbigint(31), this.rbigint(31))
    console.log("deposit", deposit)
    const note = this.toHex(deposit['preimage'], 62)
    const noteString = `hexmix-${this.erc20tornado.tokenAmount}-${1}-${note}`;
    console.log('Your commitment:', deposit['commitment'])
    console.log('Your note:', noteString)
    return [{ 'noteString': noteString, 'commitment': deposit['commitment'] }]
  }


  async depositErc20(commitment) {
    const that = this;
    let allowance = await that.erc20.methods.allowance(that.account, that.erc20tornado._address).send({ from: that.account });
    console.log(allowance);
    if (allowance) {
      console.log(`Approving ${that.erc20tornado.tokenAmount} tokens for deposit`)
      await this.erc20.methods.approve(that.erc20tornado._address, that.erc20tornado.tokenAmount).send({ from: that.account })
    }

    console.log('Submitting deposit transaction')
    await that.erc20tornado.methods.deposit(this.toHex(commitment)).send({ from: that.account, gas: 2e6 })


  }

  //withdraw

  async generateMerkleProof(contract, deposit) {
    console.log(contract.deployedBlock, deposit)
    const that = this
    // Get all deposit events from smart contract and assemble merkle tree from them
    console.log('Getting current state from tornado contract')
    const events = await contract.getPastEvents('Deposit', { fromBlock: contract.deployedBlock, toBlock: 'latest' })
    console.log(events)
    const leaves = events
      .sort((a, b) => a.returnValues.leafIndex - b.returnValues.leafIndex) // Sort events in chronological order
      .map(e => e.returnValues.commitment)
    console.log(leaves)
    const tree = new merkleTree(20, leaves)

    // Find current commitment in the tree
    let depositEvent = events.find(e => e.returnValues.commitment === that.toHex(deposit.commitment))
    let leafIndex = depositEvent ? depositEvent.returnValues.leafIndex : -1
    console.log(leafIndex)
    if(leafIndex === -1){
      alert('No Deposit with this note, Transaction failed')
    }
    // Validate that our data is correct
    const isValidRoot = await contract.methods.isKnownRoot(that.toHex(await tree.root())).call()
    const isSpent = await contract.methods.isSpent(that.toHex(deposit.nullifierHash)).call()
    assert(isValidRoot === true, 'Merkle tree is corrupted')
    assert(isSpent === false, 'The note is already spent')
    assert(leafIndex >= 0, 'The deposit is not found in the tree')

    // Compute merkle proof of our commitment
    return await tree.path(leafIndex)
  }

  async generateProof(contract, deposit, recipient, relayer = 0, fee = 0, refund = 0) {
    const that = this
    // Decode hex string and restore the deposit object
    // let buf = Buffer.from(note.slice(2), 'hex')
    // let deposit = createDeposit(bigInt.leBuff2int(buf.slice(0, 31)), bigInt.leBuff2int(buf.slice(31, 62)))

    // Compute merkle proof of our commitment
    const { root, path_elements, path_index } = await this.generateMerkleProof(contract, deposit)
    console.log("inside generate", root, path_elements, path_index)

    // Prepare circuit input
    const input = {
      // Public snark inputs
      root: root,
      nullifierHash: deposit.nullifierHash,
      recipient: bigInt(recipient),
      relayer: bigInt(relayer),
      fee: bigInt(fee),
      refund: bigInt(refund),

      // Private snark inputs
      nullifier: deposit.nullifier,
      secret: deposit.secret,
      pathElements: path_elements,
      pathIndices: path_index,
    }
    console.log("input", input)

    let proving_key: any;
    await fetch('http://127.0.0.1:8887/withdraw_proving_key.bin').then((response) => {
      return response.arrayBuffer();
    }).then((b) => {
      proving_key = b;
    });
    console.log(proving_key)
    console.log('Generating SNARK proof')
    // console.time('Proof time')
    that.groth16 = await buildGroth16()
    const proofData = await websnarkUtils.genWitnessAndProve(that.groth16, input, circuit, proving_key)
    const { proof } = websnarkUtils.toSolidityInput(proofData)
    // console.timeEnd('Proof time')
    alert("Your Snark Proof is generated Successfully!"+proof)
    console.log(proof)
    const args = [
      that.toHex(input.root),
      that.toHex(input.nullifierHash),
      that.toHex(input.recipient, 20),
      that.toHex(input.relayer, 20),
      that.toHex(input.fee),
      that.toHex(input.refund)
    ]

    return { proof, args }
  }

  parseNote(noteString) {
    const noteRegex = /hexmix-(?<amount>[\d.]+)-(?<netId>\d+)-0x(?<note>[0-9a-fA-F]{124})/g
    const match = noteRegex.exec(noteString)
    if (!match) {
      throw new Error('The note has invalid format')
    }

    const buf = Buffer.from(match.groups.note, 'hex')
    const nullifier = bigInt.leBuff2int(buf.slice(0, 31))
    const secret = bigInt.leBuff2int(buf.slice(31, 62))
    const deposit = this.createDeposit(nullifier, secret)
    const netId = Number(match.groups.netId)

    return { amount: match.groups.amount, netId, deposit }
  }

  async withdrawErc20(note, recipient) {
    const that = this;
    if (recipient) {
      let { amount, netId, deposit } = that.parseNote(note);
      console.log(deposit)
      let number = parseInt(amount)
      let no = number / 100000000
      console.log((no).toString())
      that.tornados((no).toString());
      const { proof, args } = await this.generateProof(that.erc20tornado, deposit, recipient)
      const account = await that.getAccount()
      console.log("args", args, "account", account)
      console.log('Submitting withdraw transaction')
      await that.erc20tornado.methods.withdraw(proof, ...args).send({ from: that.account, gas: 1e6 })
      console.log('Done')
      alert("Withdraw Transaction made Successfully!")
    } else {
      console.log("Exiting without taking action...");
    }
  }



}
